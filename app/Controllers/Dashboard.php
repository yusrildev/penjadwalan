<?php

namespace App\Controllers;

class Dashboard extends BaseController
{
    public function index()
    {
        $header['title'] = 'Dashboard';
        echo view('partial/header', $header);
        echo view('partial/top_menu');
        echo view('partial/side_menu');
        echo view('dashboard');
        echo view('partial/footer');
    }

    public function form()
    {
        $header['title'] = 'Form';
        echo view('partial/header', $header);
        echo view('partial/top_menu');
        echo view('partial/side_menu');
        echo view('side_menu/form');
        echo view('partial/footer');
    }
    public function calendar()
    {
        $header['title'] = 'Calendar';
        echo view('partial/header', $header);
        echo view('partial/top_menu');
        echo view('partial/side_menu');
        echo view('side_menu/calendar');
        echo view('partial/footer');
    }
}
